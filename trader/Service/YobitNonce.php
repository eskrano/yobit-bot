<?php

namespace Trader\Service;

class YobitNonce
{
    /**
     * @var string
     */
    private $path;


    private $data;


    /**
     * YobitNonce constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
        $this->load($path);
    }

    private function load($path)
    {
        $json = json_decode(
            file_get_contents($path),
            true
        );

        $this->data = $json;
    }

    public function store()
    {
        return file_put_contents(
            $this->path,
            json_encode($this->data, JSON_PRETTY_PRINT)
        );
    }

    public function get()
    {
        $actual = $this->data['actual-nonce'];
        $this->increment();
        return $actual;
    }

    public function increment($value = 1)
    {

        $this->data['last-nonce'] = $this->data['actual-nonce'];
        $this->data['actual-nonce']++;

        if (!$this->store()) {
            throw new \Exception("Cant store data to cache file");
        }

        return true;
    }
}