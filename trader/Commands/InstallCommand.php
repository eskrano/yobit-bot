<?php

namespace Trader\Commands;

use Ifsnop\Mysqldump\Mysqldump;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Trader\Service\Database;
use Trader\Service\Mysql\Import;

class InstallCommand extends Command
{
    protected function configure()
    {
        $this->setName('install');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<error>Warning! Old api tokens has been deleted</error>');

        /** @var QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');

        $keyAsk = new Question('<info>Enter fresh yobit api key: </info>', false);

        $key = $questionHelper->ask($input, $output, $keyAsk);

        $secretAsk = new Question('<info>Enter fresh yobit api secret: </info>', false);

        $secret = $questionHelper->ask($input, $output, $secretAsk);

        @unlink(sprintf("%s/yobit.json", APP_DIR));

        $data = json_encode(compact('key', 'secret'), JSON_PRETTY_PRINT);

        file_put_contents(sprintf("%s/yobit.json", APP_DIR), $data);

        //install cache.json (nonces)

        @unlink($cacheJSON = sprintf("%s/data/cache.json", APP_DIR));

        file_put_contents($cacheJSON, json_encode([
            'last-nonce' => 0,
            'actual-nonce' => 1,
        ], JSON_PRETTY_PRINT));

        $output->writeln('<info>Yobit installed</info>');

        $output->writeln('<info>Preparing to dump database</info>');

        try {

            $import = Import::import(
                $_SERVER['DB_HOST'],
                $_SERVER['DB_USER'],
                $_SERVER['DB_PASSWORD'],
                $_SERVER['DB_NAME'],
                sprintf("%s/db.sql", APP_DIR)
            );

        } catch (\Exception $e) {
            $output->writeln(sprintf("<error>Error: %s</error>", $e->getMessage()));
        }


        $output->writeln('<info>Setup completed</info>');
    }
}