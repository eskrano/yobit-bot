<?php

namespace Trader;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Trader\Service\Database;
use Trader\Service\Yobit;

class ReadyCommand extends Command
{
    protected function configure()
    {
        $this->setName('ready');
        $this->addOption('pair', 'p', InputOption::VALUE_OPTIONAL, 'pair *_{pair}', 'btc');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $start = microtime(true);

        $db = Database::getInstance();
        $yobit = Yobit::getInstance();

        $info = $yobit->getInfoPublic();

        $server_time = $info['server_time'];
        $_pairs = $info['pairs'];

        $output->writeln('<info>Public info about pairs taken</info>');

        $pairs = [];

        $output->writeln("<info>Finding related pairs</info>");
        foreach ($_pairs as $pairName => $pairData) {
            if (strpos($pairName, $input->getOption('pair')) !== false) {
                $pairs[$pairName] = $pairData;
            }
        }

        $output->writeln(sprintf("<info>Found %d related pairs</info>", count($pairs)));


        $data = [];

        $bar = new ProgressBar($output, count($pairs));
        $bar->setFormat(' <info>%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%</info>');

        foreach (array_chunk($pairs, 50, true) as $step => $pairsList) {
            //$output->writeln('<info>Getting ticker '. $step .'</info>');
            try {
                $response = $yobit->getTicker(array_keys($pairsList));

                foreach ($response as $currency => $row) {
                    $data[$currency] = $row;
                }

                $bar->advance(50);
                usleep(1500000 / 2);
            } catch (\Exception $e) {
                $output->writeln(sprintf("
                    <error>We have error! %s </error>
                ", $e->getMessage()));
            }
        }

        $bar->finish();


        $output->writeln("<info>Start update ticker database</info>");

        $pairsBar = new ProgressBar($output, count($data));
        $pairsBar->setFormat(' <info>%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%</info>');

        foreach ($data as $pairName => $pairData) {
            try {
                $count = $db->prepare('select `id` from `pairs` where `pair` = ?');
                $count->execute([
                    $pairName
                ]);

                $sell = sprintf("%.8f", $pairData['sell']);
                $buy = sprintf("%.8f", $pairData['buy']);

                if ($count->rowCount() === 0) {
                    $insert = $db->prepare('insert into `pairs` (`pair`,`sell`,`buy`,`updated_at`) VALUES (?,?,?,?)');
                    $result = $insert->execute([
                        $pairName,
                        $sell,
                        $buy,
                        $server_time
                    ]);

                    if ($result) {
                        $pairsBar->advance();
                    } else {
                        $output->writeln('<error>Cant add pair data</error>');
                    }
                } else {
                    $updateStmt = $db->prepare("update `pairs` set `sell` = ? , `buy` = ?, `updated_at` = ? where `pair` = ?");
                    $result = $updateStmt->execute([
                        $sell, $buy, $server_time, $pairName
                    ]);

                    if ($result) {
                        $pairsBar->advance();
                    } else {
                        $output->writeln('<error>Cant update pair data</error>');
                    }
                }

            } catch (\Exception $e) {
                $output->writeln("<error>" . $e->getMessage() . "</error>");
            }
        }

        // part 2 update balances

        $balances = $yobit->getFunds();

        $balanceBar = new ProgressBar($output, count($balances));
        $balanceBar->setFormat(' <info>Updating balances: %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%</info>');
        foreach ($balances as $coin => $value) {
            $updateResult = $this->updateBalance($coin, $value);

            if (true === $updateResult) {
                //$output->writeln(sprintf("<info>Balance %s updated!</info>", $coin));
            } else {
                $output->writeln(sprintf("<error>Balance %s not updated!</error>", $coin));
            }

            $balanceBar->advance();
        }

        $balanceBar->finish();

        $output->writeln(
            sprintf("<info>Script finished in: %s", microtime(true) - $start)
        );
    }

    /**
     * @param string $coin
     * @param string $value
     * @return bool
     */
    private function updateBalance(string $coin, string $value)
    {
        $db = Database::getInstance();

        $balanceQuery = $db->prepare('select * from `balance` where `coin` = ?');
        $balanceQuery->execute([
            $coin
        ]);

        if ($balanceQuery->rowCount() === 1) {
            $updateQuery = $db->prepare('update `balance` set `balance` = ?, `updated_at` = ?  where `coin` = ?');
            return $updateQuery->execute([sprintf('%.8f', $value), time(), $coin]);
        }

        $insertQuery = $db->prepare('insert into `balance`(`balance`, `coin`, `updated_at`) VALUES(?,?,?)');
        return $insertQuery->execute([
            sprintf("%.8f", $value),
            $coin,
            time()
        ]);

    }
}