<?php

namespace Trader\Service;

use Dotenv\Dotenv;

class Database
{
    /**
     * @var \PDO
     */
    private static $pdo;

    private static $dsn;

    /**
     * @return \PDO
     */
    public static function getInstance()
    {
        if (null === static::$pdo) {
            static::establishConnection();
        }
        return static::$pdo;
    }

    /**
     * @return void
     */
    private static function establishConnection()
    {
        $dsn = sprintf("mysql:host=%s;dbname=%s",
            $_SERVER['DB_HOST'],
            $_SERVER['DB_NAME']
        );

        static::$dsn = $dsn;

        static::$pdo = new \PDO($dsn, $_SERVER['DB_USER'], $_SERVER['DB_PASSWORD']);
    }

    public static function getDsn()
    {
        return static::$dsn;
    }
}