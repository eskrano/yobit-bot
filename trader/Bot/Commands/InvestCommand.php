<?php

namespace Trader\Bot\Commands;

use Trader\Bot\BaseCommand;

class InvestCommand extends BaseCommand
{
    public  function handle()
    {
        $allowedTypes = ['d','m'];

        $plan = isset($this->args[1]) ?
            (int) abs($this->args[1]->getText()) : false;

        $type = isset($this->args[2]) ?
            $this->args[2]->getText() : 'd';

        $period = isset($this->args[3]) ?
            (int) abs($this->args[3]->getText()) : 10;

        $percent = isset($this->args[4]) ?
            $this->args[4]->getText() : 10;

        var_dump($plan,$type,$period);

        if (! $plan || !$period) {
            $this->sendTyping();
            $this->telegram->sendMessage([
                'chat_id' => $this->getChatId(),
                'text' => 'Укажите количество ивестированной монеты. А также период инвестиции.',
            ]);

            return;
        }

        if (! in_array($type, $allowedTypes)) {
            return;
        }

        if ('d' === $type) {
            //daily
            $sum = $plan;

            for ($i = 0; $i < $period; $i++) {
                $update = $sum * ( $percent / 100);
                $sum += $update;
            }

            $this->sendTyping();
            $this->telegram->sendMessage([
                'chat_id' => $this->getChatId(),
                'text' => sprintf("Profit %.8f ", $sum)
            ]);
        }
    }
}