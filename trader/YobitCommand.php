<?php


namespace Trader;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Trader\Service\Database;
use Trader\Service\Yobit;
use Trader\Service\YobitCredentials;
use Trader\Service\YobitNonce;
use Trader\Traits\YobitAuthorizationTrait;

class YobitCommand extends Command
{

    protected function configure()
    {
        $this->setName('yobit:balance')
            ->addOption('balance', 'b', InputOption::VALUE_REQUIRED, false)
            ->addOption('balance_rate', 'br', InputOption::VALUE_OPTIONAL, false, 'usd');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $nonce = new YobitNonce(APP_DIR . '/data/cache.json');
        $credentials = new YobitCredentials(APP_DIR . '/yobit.json');
        $client = new Yobit($nonce, $credentials);


        $currency = $input->getOption('balance');
        $output->writeln([
            sprintf("<info>Getting balance of %s</info>", $currency),
        ]);

        $balance = sprintf("%.8f",$client->getFunds($currency));

        if (is_array($balance)) {
            $output->writeln(array_map(function ($k, $v) {
                return sprintf("<info>%s : %.8f </info>", $k, $v);
            }, array_keys($balance), array_values($balance)));
        } else {
            $output->writeln(sprintf("<info>%s</info>", $balance));
            /*
            $data = $client->getPairDepth($arrKey = sprintf("%s_%s", $currency, $input->getOption('balance_rate')));
            $asks = $data[$arrKey]['asks'];
            $bids = $data[$arrKey]['bids'];

            $output->writeln(sprintf("<info>~%.8f $</info>",
                    $balance * 0.9975 * $asks[0][0])
            );
            */

            $db = Database::getInstance();

            $stmt = $db->prepare('update `balance` set `balance` = ? where `coin` = ?');
            $update = $stmt->execute([
                sprintf("%.8f", $balance),
                $currency
            ]);

            if ($update) {
                $output->writeln('<info>Update [OK]</info>');
            } else {
                $output->writeln('<error>Update [ERROR]</error>');
            }
        }
    }
}