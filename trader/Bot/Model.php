<?php

namespace Trader\Bot;

use Trader\Service\Database;

class Model
{
    private $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function setLastUpdateId(int $id)
    {
        return (bool) file_put_contents(APP_DIR . '/update.telegram',$id);
    }

    /**
     * @return bool|string
     */
    public function getLastUpdateId()
    {
        return file_get_contents(APP_DIR . '/update.telegram');
    }
}