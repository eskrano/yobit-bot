<?php

namespace Trader\Service\Exception;

use Trader\Service\Exception\YoBitNetApiException;

class YoBitNetAPIFailureException extends YoBitNetApiException
{

}