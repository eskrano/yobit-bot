<?php

namespace Trader\Traits;

use Exception;
use Symfony\Component\Console\Output\OutputInterface;
use Trader\Service\YobitSDK;

trait YobitAuthorizationTrait
{
    protected $authKey;
    protected $authSecret;

    public function authorize($keyFile = null, OutputInterface $output)
    {
        if (null === $keyFile) {
            $keyFile = $this->getDefaultKeyFile();
        } else {
            try {

                $keyFile = $this->loadKeyFile($keyFile);
                $this->authKey = $keyFile['key'];
                $this->authSecret = $keyFile['secret-key'];
            } catch (Exception $e) {
                $output->writeln('<error>Yobit auth keyfile not found</error>');
            }
        }

        $output->writeln(sprintf("<info>Key file loaded</info>"));
        $output->writeln(sprintf("<info>Key:%s</info>", $keyFile['key']));
        $output->writeln(sprintf("<info>Secret:%s</info>", $keyFile['secret-key']));

        return [
            $keyFile['key'],
            $keyFile['secret-key']
        ];

    }

    private function loadKeyFile($path)
    {
        return json_decode(
            file_get_contents($path),
            true
        );
    }

    public function makeHash()
    {

    }


    private function getDefaultKeyFile()
    {
        return $this->loadKeyFile(__DIR__ . '/../../yobit.json');
    }
}