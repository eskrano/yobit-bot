<?php

namespace Trader\Service;

class YobitCredentials
{
    private $key;

    private $secret;

    const JSON_KEY_ATTRIBUTE = 'key';
    const JSON_SECRET_ATTRIBUTE = 'secret';

    public function __construct(string $path)
    {
        $this->load($path);
    }

    private function load(string $path)
    {
        $data = json_decode(
            file_get_contents($path),
            true
        );

        $this->key = $data[static::JSON_KEY_ATTRIBUTE] ?? null;
        $this->secret = $data[static::JSON_SECRET_ATTRIBUTE] ?? null;

    }

    public function getKey()
    {
        return $this->key;
    }

    public function getSecret()
    {
        return $this->secret;
    }
}