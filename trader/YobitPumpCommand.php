<?php

namespace Trader;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Trader\Service\YobitNonce;
use Trader\Service\YobitCredentials;
use Trader\Service\Yobit;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class YobitPumpCommand extends Command
{

    private $client;

    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->client = Yobit::getInstance();
    }

    protected function configure()
    {
        $this->setName('yobit:pump')
            ->addOption('target','t', InputOption::VALUE_REQUIRED, false)
            ->addOption('sleep', 's', InputOption::VALUE_OPTIONAL, 'Sleep times', 4);
    }

    private function getAvgPrice(string $pair)
    {
        $data = $this->client->getPairDepth($pair);

        $min_ask = isset($data[$pair]['asks'][0][0]) ?
            $data[$pair]['asks'][0][0] :
            0;

        $max_bid = isset($data[$pair]['bids'][0][0]) ?
            $data[$pair]['bids'][0][0] :
            0;

        return [
            'bids' => $max_bid,
            'asks' => $min_ask,
        ];
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = $this->client;

        $pair = $input->getOption('target');

        $depth = $client->getPairDepth($completedPair = sprintf("%s_btc",$pair ));

        if (!isset($depth[sprintf("%s_btc", $pair)])) {
            $output->writeln("<error>Bad pair</error>");
            return;
        }

        $min_ask = isset($depth[$completedPair]['asks'][0][0]) ?
            $depth[$completedPair]['asks'][0][0] :
            0;

        $max_bid = isset($depth[$completedPair]['bids'][0][0]) ?
            $depth[$completedPair]['bids'][0][0] :
            0;

        $output->writeln(sprintf("<info>Sell: %.8f</info>", $min_ask));
        $output->writeln(sprintf("<info>BID %.8f</info>", $max_bid));


        /** @var QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');
        $confirmation = $this->createConfirmationQuestion('Start pump?', false);

        if ($questionHelper->ask($input,$output, $confirmation)) {
            $output->writeln('<info>Ok! How many btc u can spent?</info>');

            $questionBtc = new Question(sprintf('Btc (by default its %.8f):',$funds = $client->getFunds('btc')));

            $ask = $questionHelper->ask($input,$output,$questionBtc);

            if ($ask > $funds) {
                $output->writeln('<error> No money </error>');
            } else {
                $trade = $client->trade($completedPair, 'buy', sprintf("%.8f",
                        $min_ask * 1.3
                    ), sprintf("%.8f", $ask));
            }
        }

        foreach (range(0,(int) $input->getOption('sleep')) as $sleep) {
            $output->writeln(sprintf('<info>Monitoring %d step of %d</info>', $sleep, $input->getOption('sleep')));
            $priceUpdated = $this->getAvgPrice($completedPair);

            $output->writeln(sprintf('<info>Bid : %.8f | Ask %.8f</info>',$priceUpdated['bids'],$priceUpdated['asks']));

            $output->writeln('<info>Sleeping 3 sec</info>');
            sleep(3);
        }

        $output->writeln('<info>Check depth</info>');

        $depthUpdated = $client->getPairDepth($completedPair);

        if (! isset($depthUpdated[$completedPair])) {
            $output->writeln('<error>Depth not found</error>');

            return;
        }

        $asks_new = $depthUpdated[$completedPair]['asks'][0][0];
        $bids_new = $depthUpdated[$completedPair]['bids'][0][0];

        $output->writeln('<info>Current price</info>');
        $output->writeln(sprintf("<info>Ask : %.8f</info>", $asks_new));
        $output->writeln(sprintf("<info>Bids: %.8f</info>", $bids_new));

        $output->writeln(sprintf("<info>Change ask : %.8f</info>",
            ($asks_new - $min_ask)
            ));

        $output->writeln(sprintf("<info>Change bid : %.8f</info>",
            ($bids_new - $max_bid)
        ));
    }

    /**
     * @param string $text
     * @param bool $defaultValue
     * @return ConfirmationQuestion
     */
    private function createConfirmationQuestion(string $text, $defaultValue = false)
    {
        return new ConfirmationQuestion($text,$defaultValue);
    }

}