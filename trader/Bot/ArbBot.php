<?php

namespace Trader\Bot;


use function Amp\File\exists;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Update;
use Trader\Service\Database;
use Trader\Service\Yobit;

class ArbBot extends Command
{

    /**
     * @var Model
     */
    private $model;

    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->model = new Model();
    }

    public function configure()
    {
        $this->setName('bot:arb');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $telegram = new Api($_SERVER['TG_API_TOKEN']);

        $mappings = require __DIR__ . '/mappings.php';

        $last_update = $this->model->getLastUpdateId();

        $params = [];

        if ($last_update !== false) $params['offset'] = $last_update + 1;

        $updates = $telegram->getUpdates($params);


        foreach ($updates as $update) {
            /** @var BaseCommand|bool $command */
            $command = (new CommandParser($mappings))->detectCommandByUpdate($update);

            if (false === $command) {
                continue;
            }

            $output->writeln(
                sprintf("<info>[%s] Command: %s | UpdateID: %d</info>", date('Y-m-d H:i:s'), $command, $update->getUpdateId())
            );
            $command->handle();


            if (end($updates) == $update) {
                $this->model->setLastUpdateId($update->getUpdateId());
            }

            unset($command,$update);
        }

        sleep(1);
    }
}