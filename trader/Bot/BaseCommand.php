<?php

namespace Trader\Bot;

use Telegram\Bot\Api;
use Telegram\Bot\Objects\Update;

abstract class BaseCommand
{
    /**
     * @var string
     */
    protected $message;

    /**
     * @var Update
     */
    public $update;


    /**
     * @var Api
     */
    protected $telegram;

    /**
     * @var CommandArg[]
     */
    protected $args;

    /**
     * BaseCommand constructor.
     * @param string $message
     * @param array $args
     */
    public function __construct(string $message, array $args = [])
    {
        $this->message = $message;
        $this->args = $args;
        $this->telegram = new Api($_SERVER['TG_API_TOKEN']);
    }

    /**
     * @return mixed
     */
    public abstract function handle();

    public function __toString()
    {
        return static::class;
    }

    protected function getChatId()
    {
        return $this->update->getMessage()->getChat()->getId();
    }

    protected function sendTyping()
    {
        $this->telegram->sendChatAction([
            'chat_id' => $this->getChatId(),
            'action' => 'typing',
        ]);
    }

}