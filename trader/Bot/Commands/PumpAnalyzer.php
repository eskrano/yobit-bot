<?php

namespace Trader\Bot\Commands;

use Trader\Bot\BaseCommand;
use Trader\Service\Yobit;

class PumpAnalyzer extends BaseCommand
{
    public  function handle()
    {

        $coin = isset($this->args[1]) ?
            $this->args[1]->getText() : false;

        if (! $coin) {
            $this->sendTyping();
            $this->telegram->sendMessage([
                'chat_id' => $this->getChatId(),
                'text' => 'Syntax /pa liza_btc',
            ]);
            return;
        }


        $yobit = Yobit::getInstance();

        try {

            $depth = $yobit->getPairDepth($coin);


           // var_dump($depth);

        } catch (\Exception $e) {
            $this->sendTyping();
            $this->telegram->sendMessage([
                'chat_id' => $this->getChatId(),
                'text' => 'Pair not found',
            ]);

            return;
        }

        $asks = $depth[$coin]['asks'];
        $bids = $depth[$coin]['bids'];


        if (count($asks) < 1) return;


        $startPrice = $asks[0][0];

        $target_x3 = $startPrice * 3;
        $target_x5 = $startPrice * 5;
        $target_x3_reach = $target_x5_reach =  false;
        $target_x3_volume = $target_x5_volume = 0;

        $sums = [
            'x3' => [],
            'x5' => [],
        ];

        foreach ($asks as $price => $volume) {
            if (! $target_x3_reach) {
                $target_x3_volume = floatval($price) * floatval($volume);

                if (array_sum($sums['x3']) >= $target_x3) {
                    $target_x3_reach = true;
                } else {
                    $sums['x3'][] = sprintf("%.8f", $price *  $volume);
                }
            }

            if (! $target_x5_reach ) {
                $target_x5_volume = floatval($price) * floatval($volume);

                if (array_sum($sums['x5']) >= $target_x5) {
                    $target_x5_reach = true;
                } else {
                    $sums['x5'][] = sprintf("%.8f", $price *  $volume);
                }
            }
        }

        $message = "Анализ монеты %s \n";
        $message .= "Для достижения х3 уровня нужно: \n";
        $message .= "Обьем+ : %.8f %s стоимость монеты ~ %.8f\n";
        $message .= "Для достижения х5 уровня нужно: \n";
        $message .= "Обьем+ : %.8f %s стоимость монеты ~ %.8f\n";



        var_dump($sums);


        $text = sprintf($message,
                $coin,
                array_sum($sums['x3']),
                explode('_', $coin)[1],
                $target_x3,
                array_sum($sums['x5']),
                explode('_', $coin)[1],
                $target_x5
            );

        $this->telegram->sendMessage([
            'chat_id' => $this->getChatId(),
            'text' => $text,
        ]);
    }
}