<?php

namespace Trader\Bot;

class CommandArg
{
    /**
     * @var string
     */
    protected $text;

    /**
     * @var bool
     */
    protected $is_start;

    /**
     * CommandArg constructor.
     * @param string $text
     * @param bool $is_start
     */
    public function __construct(string $text, bool $is_start = false)
    {
        $this->text = $text;
        $this->is_start = $is_start;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return bool
     */
    public function isStart()
    {
        return $this->is_start;
    }
}