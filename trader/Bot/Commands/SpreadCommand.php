<?php

namespace Trader\Bot\Commands;

use Trader\Bot\BaseCommand;
use Trader\Service\Yobit;

class SpreadCommand extends BaseCommand
{
    public  function handle()
    {
        $pair = isset($this->args[0]) ?
            $this->args[0]->getText() : false;

        if (! $pair) {
            $this->telegram->sendMessage([
                'chat_id' => $this->getChatId(),
                'text' => 'Укажите пару liza_rur',
            ]);

            return;
        }

        $yobit = Yobit::getInstance();

        $yobit->getPairDepth($pair);
        $yobit->getPairDepth($pair);
    }
}