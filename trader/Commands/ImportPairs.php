<?php

namespace Trader\Commands;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Trader\Service\Yobit;

class ImportPairs extends Command
{
    protected function configure()
    {
        $this->setName('import:pairs');
        $this->addOption('pair','p', InputOption::VALUE_REQUIRED, 'pair','_btc');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $pair = $input->getOption('pair');

        $yobit = Yobit::getInstance();
        $data = $yobit->getInfoPublic();

        $fp = [];

        foreach ($data['pairs'] as $name => $data) {
            if (strpos($pair, $input->getOption('pair')) !== false) {
                $fp[] = explode('_', $name)[0];
            }
        }

        $contents = json_encode($fp);
        file_put_contents(sprintf("%s/import_pairs_%s.json", APP_DIR,$pair), $contents);
    }
}