<?php

namespace Trader\Bot\Commands;

use Trader\Bot\BaseCommand;

class TestCommand extends BaseCommand
{
    public  function handle()
    {
        $this->telegram->sendMessage([
            'chat_id' => $this->update->getMessage()->getChat()->getId(),
            'text' => 'echo',
        ]);
    }
}