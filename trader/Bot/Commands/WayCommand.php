<?php

namespace Trader\Bot\Commands;

use Trader\Bot\BaseCommand;
use Trader\Service\Yobit;

class WayCommand extends BaseCommand
{
    public function handle()
    {
        $yobit = Yobit::getInstance();

        $coin = $this->args[1]->getText();
        if (isset($this->args[2])) {
            $currency = $this->args[2]->getText();
        } else {
            $currency = 'btc';
        }

        $markets = [
            'doge', 'eth', 'btc', 'rur', 'usd', 'waves',
        ];

        $this->telegram->sendMessage([
            'chat_id' => $this->getChatId(),
            'text' => sprintf("Finding way %s ~ %s", $this->args[1]->getText(), $currency),
        ]);


        $pairs = [];

        foreach ($markets as $mark) {
            $pairs[] = sprintf("%s_%s", $coin, $mark);
        }

        $pairs_ex = [];
        foreach ($markets as $market) {
            $pairs_ex[] = sprintf("%s_%s", $market, $currency);
            $pairs_ex[] = sprintf("%s_%s", $currency, $market);
        }

        $this->sendTyping();
        try {
            $ticker_ex = $yobit->getTicker($pairs_ex);
            sleep(1);
            $ticker = $yobit->getTicker($pairs);
        } catch (\Exception $e) {
            return;


        }


        $message = '';

        foreach ($ticker as $p => $tick) {

            $mrkt = explode('_', $p)[1];

            if ($mrkt !== $currency) {
                $curr = isset($ticker_ex[sprintf('%s_%s', $currency, $mrkt)]) ?
                    $ticker_ex[sprintf('%s_%s', $currency, $mrkt)] : false;

                if (!$curr) {
                    // reverse curr

                    $curr = isset($ticker_ex[sprintf('%s_%s', $mrkt, $currency)]) ?
                        $ticker_ex[sprintf('%s_%s', $mrkt, $currency)] : false;

                    if (!$curr) {
                        continue;
                    }
                }
                if ($mrkt === 'rur') {
                    $estimate = $tick['buy'] / $curr['sell'];

                } else {
                    $estimate = $curr['buy'] * $tick['sell'];
                }

                $message .= sprintf("#%s \n📈Buy: %8.f \n📉Sell: %8.f \n", $p, $tick['buy'], $tick['sell']);
                $message .= sprintf("(Est %8.f %s) \n", $estimate, $currency);
            } else {
                $message .= sprintf("#%s \n📈Buy: %8.f \n📉Sell: %8.f \n", $p, $tick['buy'], $tick['sell']);
            }
        }

        $this->telegram->sendMessage([
            'chat_id' => $this->getChatId(),
            'text' => trim($message),
            'parse_mode' => 'html',
        ]);

        /**
         * $this->sendTyping();
         *
         * $_pairs = [];
         *
         * foreach ($markets as $market) {
         * if ($market == 'rur') {
         * $_pairs[] = sprintf("%s_%s", $currency,$market);
         * } else {
         * $_pairs[] = sprintf("%s_%s", $market, $currency);
         * }
         * }
         *
         *
         *
         * $data = $yobit->getTicker($_pairs);
         *
         * $basePrice = $ticker[sprintf('%s_%s', $coin, $currency)];
         */

    }
}