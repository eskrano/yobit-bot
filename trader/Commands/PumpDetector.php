<?php

namespace Trader\Commands;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Trader\Service\Yobit;
use Telegram\Bot\Api;

class PumpDetector extends Command
{
    const CHAT = '-1001217908643';

    private $telegram;

    private $input;

    private $stoplist = [
        'liza','vntx','doge','taxi','dft',
    ];

    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->telegram = new Api($_SERVER['TG_API_TOKEN']);
    }

    protected function configure()
    {
        $this->setName('pump:detect');
        $this->addOption('pair', 'p', InputOption::VALUE_REQUIRED, '_btc', '_btc');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command_start = microtime(true);

        $pair = $input->getOption('pair');
        $this->input = $input;
        $yobit = Yobit::getInstance();
        $debug = [];


        if (file_exists(APP_DIR . '/pairs_' . $pair . '.json')) {
            $data = json_decode(
                file_get_contents(APP_DIR . '/pairs_' . $pair . '.json'), true
            );

            $debug['pairs_load'] = 'cache';
        } else {
            $data = $yobit->getInfoPublic();
            file_put_contents(APP_DIR . '/pairs_' . $pair . '.json', json_encode($data));
            $debug['pairs_load'] = 'api';
        }

        $filteredData = [];


        foreach ($data['pairs'] as $pair => $pdata) {
            if (strpos($pair, $input->getOption('pair')) !== false) {
                $filteredData[$pair] = $pdata;
            }
        }


        $data['pairs_count'] = count($filteredData);

        $_data = [];


        $bar = new ProgressBar($output, count($filteredData));
        $bar->setFormat(' <info>%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%</info>');

        foreach (array_chunk($filteredData, 50, true) as $step => $pairsList) {
            //$output->writeln('<info>Getting ticker '. $step .'</info>');
            try {
                // exit(var_dump(array_keys($pairsList)));
                $response = $yobit->getTicker(array_keys($pairsList));

                if (is_array($response) && count($response) > 0) {
                    foreach ($response as $currency => $row) {
                        $_data[$currency] = $row;
                    }
                }
                usleep(1000000 / 2);
            } catch (\Exception $e) {
                $output->writeln(sprintf("
                    <error>We have error! %s </error>
                ", $e->getMessage()));
            }
            $bar->advance(50);
        }


        $bar->finish();

        $cachePath = APP_DIR . '/cache_' . $input->getOption('pair') . '.yobit.json';

        if (file_exists($cachePath)) {
            $lasts = file_get_contents($cachePath);
            $lasts = json_decode($lasts, true);
        } else {
            $lasts = [];
        }

        foreach ($_data as $curr => $info) {
            $a = $info['high'];
            $b = $info['low'];
            $last = $info['last'];

            /*
            if ($last == 0 || $a == 0) {
                $res = 0;
            } else {
                $res = (($a - $last) / $a) * 100;
            }*/

            if (!isset($lasts[$curr])) {
                $lasts[$curr] = $info;
            }

            $old = $lasts[$curr];

            if ($info['updated'] < (time() - 40)) {
                continue; // no update for this currency
            }

            if ($old['last'] == 0) {
                $change = 0;
            } else {
                $change = ($info['last'] - $old['last']) / (($info['last'] + $old['last']) / 2) * 100;
            }


            if ($change > 0) {
                $up = true;
                $down = false;
            } elseif ($change < 0) {
                $up = false;
                $down = true;
            } else {
                $up = $down = false;
            }

            if ($info['vol'] > $old['vol']) {
                $volChange = $info['vol'] - $old['vol'];
                $volChangePercent = ($info['vol'] - $old['vol']) / (($info['vol'] + $old['vol']) / 2) * 100;
            } else {
                $volChange = 0;
                $volChangePercent = 0;
            }


            $volLDetect = [
                '_rur' => 3500,
                '_btc' => 0.005,
            ];

            if (floatval($volChange) >= floatval($volLDetect[$input->getOption('pair')])) {
                //floatval($change) > floatval(2) || floatval($change) < floatval(-10)
                if (floatval($change) > floatval(2) || floatval($change) < floatval(-10)) {

                    /*
                    if (floatval($change) > 10) {
                        $up = true;
                        $down = false;
                    } else {
                        $up = false;
                        $down = true;
                    }*/

                    $styleBlock = $down === true ? 'error' : 'info';

                    $output->writeln(sprintf("<%s>Currency %s price changed for %.2f percents | Vol changed for %.8f <%s>",
                        $styleBlock, $curr, $change, $volChange, $styleBlock
                    ));



                    $this->sendNotification($curr, $info, $old, $change, $up, $down, $volChange, $volChangePercent);
                }
            }
        }


        // save cache


        // @unlink($cachePath);

        $cacheSave = file_put_contents(
            $cachePath,
            json_encode($_data)
        );

        $debug['cache_save'] = $cacheSave;
        $debug['exec_time'] = microtime(true) - $command_start;

        $to = static::CHAT;

        // $this->sendDebug($debug, $to);
    }

    private function sendDebug(array $debug = [], $to)
    {
        $text = "Execution time : %s\n";
        $text .= "Pairs load from: %s\n";

        $text = sprintf($text,
            round($debug['exec_time'], 3),
            $debug['pairs_load'] == 'api' ? 'API' : "Cache"
        );


        $this->telegram->sendMessage([
            'chat_id' => $to,
            'text' => $text,
        ]);
    }

    private function sendNotification($coin, $info, $old, $change, $isPump = false, $isDump = false, $volChange = false, $volChangePercent = false)
    {

        $cName = explode('_',$coin)[0];

        if (in_array($cName,$this->stoplist)) {
            return true; //if stoplist
        }

        $text = "❗️❗️❗️Обнаружен %s! Пара %s ❗️❗️❗️\n";
        $text .= "❗️❗️❗️https://yobit.net/ru/trade/%s/%s ❗️❗️❗️ \n";
        $text .= "❗️❗️❗️https://yobit.io/ru/trade/%s/%s ❗️❗️❗️ \n";
        $text .= "🔴 Vol+: %.8f \n";
        $text .= "🔴 <b>24H VOL:</b> %.2f \n";
        $text .= "🔴 Old price: %.8f \n";
        $text .= "🔴 New price: %.8f \n";
        $text .= "🔴 Percent change:  %s%.2f \n";
        $text .= "🔴 Vol change percent: %.2f 📈 \n";


        /**
         * getting trade dominance test.
         */

        $yobit = Yobit::getInstance();
        $trades = $yobit->trades($coin);
        $trades = isset($trades[$coin]) ? $trades[$coin] : false;

        if ($trades) {
            $asksCount = $bidsCount = 0;

            foreach ($trades as $trade) {
                if ($trade['type'] == 'bid') {
                    $bidsCount++;
                } else {
                    $asksCount++;
                }
            }

            $dominance = $asksCount > $bidsCount ? 'Sell' : 'Buy';

            $text .= " 🔥 Trade Dominance: <b>". $dominance. "</b>\n";
        }

        ///

        $text = sprintf($text,
            $isDump === true ? '<b>Дамп</b>' : '<b>Памп</b>',
            $coin,
            strtoupper(explode("_", $coin)[0]),
            strtoupper(explode("_", $coin)[1]),
            strtoupper(explode("_", $coin)[0]),
            strtoupper(explode("_", $coin)[1]),
            $volChange,
            $info['vol'],
            $old['last'],
            $info['last'],
            $isDump === true ? '📉' : '📈',
            $change,
            $volChangePercent
        );

        $this->telegram->sendMessage([
            'chat_id' => self::CHAT,
            'text' => $text,
            'parse_mode' => 'html',
        ]);


        $payers = [
            'ruseke23mar' => 313406722,
            'marina5april' => 419625818,
           // 'anna30mar' => 370309214,
            //'andyDtest' => 144441960,
           // 'sanyatest' => 302123978,
            'eugene' => 133529063,
            'vovankl' => 319101942,
            'vova' => 181905205,
            //'imrantest1day' => 551834351,
            //'Belarus_freedom_bjb19martest' => 534775633,
            //'bless1210test1day' => 206792976,
            //'kpumptest1day' => 546264945,
            //'dmi' => 385490679,
            //'soyo' => 74309665,
            //'vicoven' => 250961943,
            //'egor7days' => 339874311,
        ];

       // $notifiers = file_get_contents(APP_DIR . '/notifiers.json');
       // $payers = json_encode($notifiers,true);

        foreach ($payers as $name => $chatid) {
            $this->telegram->sendMessage([
                'chat_id' => $chatid,
                'text' => $text,
                'parse_mode' => 'html', //add mrkdwn
            ]);
        }

        /*
         $toronto = 231316554;

        $this->telegram->sendMessage([
            'chat_id' => $toronto,
            'text' => $text,
        ]);

        $soyo = 74309665;

        $this->telegram->sendMessage([
            'chat_id' => $soyo,
            'text' => $text,
        ]);*/
    }

}