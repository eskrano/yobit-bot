<?php

declare(strict_types=1);

namespace Trader;

use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Trader\Traits\YobitAuthorizationTrait;

class WatchCommand extends Command
{

    use YobitAuthorizationTrait;

    private $bidCount = 0;
    private $bidSum = 0;
    private $bidAvg = 0;

    private $askCount = 0;
    private $askSum = 0;
    private $askAvg = 0;

    private $limit = 2000;

    protected function configure()
    {
        $this->setName('trader:watch')
            ->setDescription('Watch specified currencies')
            ->addOption(
                'currency',
                'c',
                InputOption::VALUE_REQUIRED,
                'currencies for watch eg: lts_btc'
            )
            ->addOption(
                'hide_response',
                'h_r',
                InputOption::VALUE_OPTIONAL,
                'Hide response option',
                false
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currency = $input->getOption('currency');

        $output->setDecorated(true);
        $output->writeln(sprintf("<info>Start watch currency %s</info>", $currency));

        $this->watch($currency, $input, $output);

        $output->writeln("<info>Info succesfully updated</info>");
        $output->writeln("<info>Exiting [ok]</info>");

    }


    private function watch(string $currency, InputInterface $i, OutputInterface $o)
    {
        $httpClient = new Client([
            'base_uri' => 'https://yobit.net/api/3/trades/',
        ]);

        $response = $httpClient->get(sprintf("%s?limit=%d",$currency,$this->limit));

        if ($response->getStatusCode() !== 200) {
            $o->writeln(sprintf("<error>YoBit api response code %d</error>", $response->getStatusCode()));
            return;
        }

        $data = $response->getBody()->getContents();
        $data = json_decode($data, true);
        $preparedData = $this->prepareData($data[$currency]);

        if ($i->getOption('hide_response') !== false) {
            $fields = [
                'type',
                'price',
                'amount',
                'tid',
                'timestamp'
            ];

            $table = new Table($o);
            $table->setHeaders($fields);
            $table->setRows($preparedData);
            $table->render();
        }

        $tableStatistics = new Table($o);
        $tableStatistics->setHeaders([
            'Type', 'Avg', 'Count', 'Sum',
        ]);

        $this->askAvg = floatval($this->askSum / $this->askCount);
        $this->bidAvg = floatval($this->bidSum / $this->bidCount);

        $tableStatistics->setRows([
            [
                'Ask',
                $this->askAvg,
                $this->askCount,
                $this->askSum,
            ],
            [
                'Bid',
                $this->bidAvg,
                $this->bidCount,
                $this->bidSum,
            ]
        ]);

        $tableStatistics->render();

        $this->authorize(null, $o);

    }

    private function prepareData($data)
    {
        return array_map(function ($item) {
            $this->{$item['type'] . 'Count'}++;
            $this->{$item['type'] . 'Sum'} += $item['price'];


            $item['price'] = sprintf("%.8f", $item['price']);

            return $item;
        }, $data);
    }
}