<?php

namespace Trader;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Process;
use Trader\Service\Database;
use Trader\Service\YobitNonce;
use Trader\Service\Yobit;
use Trader\Service\YobitCredentials;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DummyCommand extends Command
{
    /**
     * @var Yobit
     */
    private $client;

    public function __construct($name = null)
    {
        parent::__construct($name);
        $nonce = new YobitNonce(APP_DIR . '/data/cache.json');
        $credentials = new YobitCredentials(APP_DIR . '/yobit.json');
        $client = new Yobit($nonce, $credentials);
        $this->client = $client;
    }

    protected function configure()
    {
        $this->setName('dummy')
            ->addOption('code','c', InputOption::VALUE_REQUIRED, 'Code', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

    }
}