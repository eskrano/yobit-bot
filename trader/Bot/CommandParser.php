<?php

namespace Trader\Bot;

use Telegram\Bot\Objects\Update;

class CommandParser
{
    /**
     * @var array
     */
    private $mappings = [];

    /**
     * CommandParser constructor.
     * @param array $mappings
     */
    public function __construct(array $mappings)
    {
        $this->mappings = $mappings;
    }


    /**
     * @param string $message
     * @return bool
     */
    public function detectCommandByMessage($message)
    {
        if (null === $message) {
            return false;
        }

        foreach ($this->mappings as $command => $commandClass) {
            if ($message === $command || strpos($message,$command) !== false) {

                $args = explode(' ', $message);

                $_args = [];

                foreach ($args as $key => $arg) {
                    $_args[] = new CommandArg($arg,$key === 0);
                }

                return new $commandClass($message, $_args);
            }
        }
        return false;
    }

    public function detectCommandByUpdate(Update $update)
    {
        if (null === $update->getMessage()) {
            return false;
        }

        $command = $this->detectCommandByMessage(
            $update->getMessage()->getText()
        );

        if (false === $command) {
            return false;
        }

        $command->update = $update;

        return $command;
    }
}