<?php

namespace Trader\Service;

class YobitOrderStatus
{
    const STATUS_ACTIVE = 0;
    const STATUS_COMPLETED_AND_CLOSED = 1;
    const STATUS_CANCEL = 2;
    const STATUS_CANCEL_AND_COMPLETED = 3;

    /**
     * @var array
     */
    public static $order2text = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_COMPLETED_AND_CLOSED => 'Completed and closed',
        self::STATUS_CANCEL => 'Cancelled',
        self::STATUS_CANCEL_AND_COMPLETED => 'Cancelled but some complete',
    ];

    /**
     * @param int $status
     * @return mixed
     */
    public static function toString(int $status)
    {
        return static::$order2text[$status];
    }
}