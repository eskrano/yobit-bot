<?php

namespace Trader\Service;

use GuzzleHttp\Client;
use Trader\Service\Exception\YoBitNetAPIInvalidParameterException;

class Yobit
{
    private $nonce;
    private $apiKey;
    private $apiSecret;

    const API_V3_ENDPOINT = 'https://yobit.net/api/3/';
    const API_TRADE_ENDPOINT = 'https://yobit.net/tapi/';

    const ORDER_TYPE_BUY = 'buy';
    const ORDER_TYPE_SELL = 'sell';

    /**
     * @return static
     */
    public static function getInstance()
    {
        $nonce = new YobitNonce(APP_DIR . '/data/cache.json');
        $credentials = new YobitCredentials(APP_DIR . '/yobit.json');
        return new static($nonce, $credentials);
    }

    public function __construct(YobitNonce $nonce, YobitCredentials $credentials)
    {
        $this->nonce = $nonce;
        $this->apiKey = $credentials->getKey();
        $this->apiSecret = $credentials->getSecret();
    }

    public function getInfo()
    {
        return $this->request('getInfo', [], static::API_TRADE_ENDPOINT);
    }

    /**
     * @param array $pairs
     * @return mixed
     */
    public function getTicker(array $pairs)
    {

        $queryAddon = strtolower($this->buildQueryAddon($pairs));
        $url = sprintf("ticker/%s?ignore_invalid=1", $queryAddon);


        return $this->request($url, [], static::API_V3_ENDPOINT);
    }

    /**
     * @return mixed
     */
    public function getInfoPublic()
    {
        return $this->request('info', [], static::API_V3_ENDPOINT);
    }


    /**
     * Get last trades.
     *
     * @param $pairs
     * @return mixed
     */
    public function trades($pairs)
    {
        if (is_array($pairs)) {
            $pairs = array_values($pairs);
            $qAdd = $this->buildQueryAddon($pairs);
        } else {
            $qAdd = $pairs;
        }

        return $this->request(sprintf("trades/%s?ignore_invalid=1", $qAdd), [], static::API_V3_ENDPOINT);
    }

    private function buildQueryAddon(array $pairs)
    {
        $query = '';

        foreach ($pairs as $v) {
            if (end($pairs) === $v) {
                $query .= sprintf("%s", $v);
            } else {
                $query .= sprintf("%s-", $v);
            }
        }

        return $query;
    }


    public function activateYobitcode(string $coupon)
    {
        $response = $this->request('RedeemCoupon', compact('coupon'), static::API_TRADE_ENDPOINT);

        return $response;
    }


    public function createYobicode(string $currency, string $amount)
    {
        $response = $this->request('CreateYobicode', compact('currency', 'amount'),
            static::API_TRADE_ENDPOINT
        );

        return $response;
    }

    public function getFunds(string $currency = '*')
    {
        $response = $this->request('getInfo', [], static::API_TRADE_ENDPOINT);

        if ($currency === '*') {
            return $response['return']['funds'];
        }

        if (isset($response['return']['funds'][$currency])) {
            return $response['return']['funds'][$currency];
        }

        return $response['return']['funds'];
    }

    public function getPairDepth(string $pair = 'btc_usd')
    {
        $contents = $this->request(sprintf("depth/%s", $pair), [], static::API_V3_ENDPOINT);

        return $contents;
    }

    public function getPairsDepth(array $pairs)
    {
        $stringifyPairs = strtolower($this->buildQueryAddon($pairs));


        $contents = $this->request(
            sprintf("depth/%s?ignore_invalid=1", $stringifyPairs),
            [],
            static::API_V3_ENDPOINT
        );

        return $contents;
    }

    /**
     * @param string $pair
     * @param string $type
     * @param string $rate
     * @param string $amount
     * @return mixed
     */
    public function createOrder(string $pair, string $type, string $rate, string $amount)
    {
        $request = $this->request('Trade', compact('pair', 'type', 'rate', 'amount'), static::API_TRADE_ENDPOINT);

        return $request;
    }

    /**
     * @param string $pair
     * @param string $type
     * @param string $rate
     * @param string $amount
     * @return mixed
     * @throws YoBitNetAPIInvalidParameterException
     */
    public function trade(string $pair, string $type, string $rate, string $amount)
    {
        if (!in_array($type, [
            static::ORDER_TYPE_BUY,
            static::ORDER_TYPE_SELL,
        ])) {
            throw new YoBitNetAPIInvalidParameterException(sprintf("Type parameter invalid. Given %s", $type));
        }

        $response = $this->request(
            sprintf("Trade"),
            compact('pair', 'type', 'rate', 'amount'),
            static::API_TRADE_ENDPOINT
        );

        return $response;
    }

    /**
     * @param string $pair
     * @return mixed
     */
    public function activeOrders(string $pair)
    {
        return $this->request(
            "ActiveOrders",
            compact('pair'),
            static::API_TRADE_ENDPOINT
        );
    }

    public function cancelOrder(string $order_id)
    {
        return $this->request('CancelOrder', [
            'order_id' => $order_id
        ], static::API_TRADE_ENDPOINT);
    }

    /**
     * @param string $order_id
     * @return mixed
     */
    public function getOrder(string $order_id)
    {
        return $this->request(
            sprintf("OrderInfo"),
            compact('order_id'),
            static::API_TRADE_ENDPOINT
        );
    }

    private function request(string $method, array $params = [], string $endpoint = self::API_V3_ENDPOINT)
    {


        if ($endpoint === static::API_TRADE_ENDPOINT) {
            $params['method'] = $method;
            $params['nonce'] = $this->nonce->get();

            $post_data_string = http_build_query($params, '', '&');

            $sign = hash_hmac('sha512', $post_data_string, $this->apiSecret);


            $headers = [
                'Sign' => $sign,
                'Key' => $this->apiKey,
            ];
        }

        $client = new Client([
            'base_uri' => $endpoint,
        ]);


        if ($endpoint == static::API_TRADE_ENDPOINT) {
            $response = $client->post('', [
                'form_params' => $params,
                'headers' => $headers,
                'force_ip_resolve' => 'v4',
            ]);
        } else {
            $response = $client->get(sprintf("%s", $method), [
                'force_ip_resolve' => 'v4',
            ]);
        }

        $response = $response->getBody()->getContents();
        $data = json_decode($response, true);

        if (isset($data['success']) && $data['success'] == 0) {
            throw new \Exception(sprintf("Error %s", $data['error']));
        }

        return $data;
    }
}