<?php

namespace Trader;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Trader\Service\Database;
use Trader\Service\Yobit;
use Trader\Service\YobitOrderStatus;

class PumpCommand extends Command
{

    /**
     * @var Yobit
     */
    private $yobit;

    /**
     * @var \PDO
     */
    private $db;

    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->yobit = Yobit::getInstance();
        $this->db = Database::getInstance();
    }

    protected function configure()
    {
        $this->setName('pump');
        $this->addOption('coin', 'c', InputOption::VALUE_REQUIRED);
        $this->addOption('pair', 'p', InputOption::VALUE_REQUIRED, 'Pair', 'btc');
        $this->addOption('coef', 'cf', InputOption::VALUE_REQUIRED, 'Coef for buy', 1.7);
        $this->addOption('hold', 'hl', InputOption::VALUE_REQUIRED, 'hold iterations 1 = 3sec', 3);
        $this->addOption('balance', 'b', InputOption::VALUE_REQUIRED, 'balance part', 0.02);
        $this->addOption('auto_sell','as', InputOption::VALUE_OPTIONAL,'autosell ?', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$startbalance = $this->yobit->getFunds($input->getOption('pair'));

        $time = microtime(true);

        $qBalance = $this->db->prepare('select * from `balance` where `coin` = ?');
        $qBalance->execute([
            $input->getOption('pair')
        ]);
        $resultBalance = $qBalance->fetch(\PDO::FETCH_ASSOC);

        $balance = $resultBalance;
        $startbalance = $balance['balance'];
        $balance['balance'] *= $input->getOption('balance');

        $output->writeln(sprintf('<info>Balance %.8f</info>', $balance['balance']));




        $pair = sprintf("%s_%s", $input->getOption('coin'), $input->getOption('pair'));

        /*
        $balance = $this->db->prepare('select * from `balance` where `coin` = ?');
        $balance->execute([
            $input->getOption('pair')
        ]);
        $balance = $balance->fetch(\PDO::FETCH_ASSOC);

        if ($startbalance != $balance['balance']) {
            $balance['balance'] = $startbalance;
        }*/

        // $output->writeln(sprintf("<info>Your %s balance === %s", $input->getOption('pair'), $balance['balance']));

        //  $output->writeln('<info>Finding last course</info>');
        //$output->writeln(sprintf("<info>Pair: %s</info>", $pair));
        $find = $this->db->prepare("select * from `pairs` where `pair` = ?");
        $find->execute([$pair]);

        if ($find->rowCount() > 0) {
            $data = $find->fetch(\PDO::FETCH_ASSOC);
            $buyPrice = sprintf("%.8f", $data['sell'] * $input->getOption('coef'));
            $output->writeln(sprintf("<info>Buy price %s</info>", $buyPrice));


            /*     $output->writeln(sprintf("
                     <info>Placing buy order</info> \n
                     <info>Pair: %s</info> \n
                     <info>Buy price: %s</info> \n
                    <info>Spent: %.8f</info>
                 ", $pair, $buyPrice, $balance['balance'])); */


            $totalCanBuy = sprintf("%.8f", ($balance['balance'] * 0.9) / $buyPrice);


            $output->writeln(sprintf("<info>Total can buy %s coins </info>", $totalCanBuy));


            //$helper = $this->getHelper('question');
            //$question = new ConfirmationQuestion('Send order?', false);

            //if (!$helper->ask($input, $output, $question)) {
            //  $output->writeln('<error>Cancelled</error>');
            //return;
            //}

            $tradeReq = microtime(true);
            $tradeResponse = $this->yobit->trade($pair, Yobit::ORDER_TYPE_BUY, $buyPrice, $totalCanBuy);

            $output->writeln(sprintf("<info>Trade request sent in %s seconds!</info>",
                    microtime(true) - $tradeReq
                ));

            if ($tradeResponse['success'] === 0) {
                $output->writeln('<error>Cant place buy order</error>');
            } else {
                $orderId = $tradeResponse['return']['order_id'];
                $received = $tradeResponse['return']['received'];
                $remains = $tradeResponse['return']['remains'];

                $output->writeln(sprintf("<info> Order Placed | ID: %d | Received : %s | Remains : %s", $orderId, $received, $remains));

                $output->writeln('<info>Sleeping 2 sec </info>');

                sleep(2);

                $newBalance = $this->yobit->getFunds($input->getOption('pair'));
                if ($newBalance < $startbalance) {
                    $output->writeln(sprintf("<info> Balance is changed %.8f</info>", $newBalance));
                    $output->writeln('<info>checking order status</info>');
                    sleep(1);

                    $orderInfo = $this->yobit->getOrder($orderId);
                    $orderInfo = $orderInfo['return'][$orderId];

                    $output->writeln(sprintf("<info>Current order status: [%s]</info>",
                        YobitOrderStatus::toString($orderInfo['status'])
                    ));

                    $last_depth_sell = 0;

                    for ($i = 0; $i < $input->getOption('hold'); $i++) {
                        $depth = $this->yobit->getPairDepth($pair);
                        $now_price = $depth[$pair]['bids'][0];

                        $output->writeln(sprintf("<info>Current price: %.8f | Est profit ~ %s</info>", $now_price[0],
                            sprintf("%.8f", $now_price[0] - $data['sell'])
                        ));
                        $output->writeln(sprintf('<info>Sleeping 1 sec | Step %d of %d</info>', $i, $input->getOption('hold')));
                        $last_depth_sell = $now_price[0];

                        if (($i + 1) !== $input->getOption('hold')) {
                            sleep(1); //if its last step we dont sleep process
                        }
                    }

                    if ($input->getOption('auto_sell') == true) {

                        $output->writeln(sprintf("<info>Last buy price is: %.8f</info>", $last_depth_sell));

                        /*$sell_price = sprintf("%.8f", sprintf("%.8f,",$last_depth_sell) -

                            sprintf("%.8f",5 / 100000000)); // hardcoded todo change this shit*/

                        $sell_price = sprintf('%.8f',
                            floatval($last_depth_sell) * 0.95
                        );

                        $output->writeln(sprintf("
                        <info>Sell price is : %s</info>
                    ", $sell_price));

                        $output->writeln("<info>Getting funds of coin</info>");
                        //$fundsCoin = sprintf("%.8f", $this->yobit->getFunds($input->getOption('coin')));
                        $fundsCoin = sprintf("%.8f", $totalCanBuy * 0.98);
                        $output->writeln(sprintf("Balance %s = %s", $input->getOption('coin'), $fundsCoin));
                        $orderResult = $this->yobit->trade($pair, Yobit::ORDER_TYPE_SELL, $sell_price, $fundsCoin);

                        if ($orderResult['success'] == 1) {
                            // if order placed
                            $output->writeln('<info>Order placed</info>');
                        } else {
                            $output->writeln('<error>Error placing order</error>');
                        }
                    } else {
                        $output->writeln("autosell disabled");
                    }
                }

            }
        } else {
            $output->writeln("<error>Pair not found</error>");
        }

    }
}