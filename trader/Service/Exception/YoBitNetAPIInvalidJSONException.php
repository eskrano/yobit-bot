<?php

namespace Trader\Service\Exception;

use Trader\Service\Exception\YoBitNetApiException;

class YoBitNetAPIInvalidJSONException extends YoBitNetApiException
{

}