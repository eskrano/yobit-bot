<?php

namespace Trader\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Trader\Service\Yobit;

class FlashOrderCommand extends Command
{
    protected function configure()
    {
        $this->setName('flash:order')
            ->addOption('pair', 'p', InputOption::VALUE_REQUIRED)
            ->addOption('hold','hld', InputOption::VALUE_OPTIONAL, "hold", 3);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pair = $input->getOption('pair');
        $yobit = Yobit::getInstance();

        $output->writeln(sprintf("<info>Pair %s</info>", $pair));

        // get depth

        $depth = $yobit->getPairDepth($pair);

        if (! isset($depth[$pair])) {
            exit(0);
        }

        $minAsk = $depth[$pair]['asks'][0][0];
        $cost = floatval($minAsk) - floatval(0.00000001);

        $output->writeln(sprintf("Min ask : %.8f , put bid order on %.8f",
                $minAsk,$cost
            ));

        $order = $yobit->trade($pair, Yobit::ORDER_TYPE_BUY,
                sprintf("%.8f", $cost),
                0.1
            );

        $order_id = $order['return']['order_id'];

        $output->writeln(sprintf("<info>Order ID  %s | SLEEP: %d sec</info>", $order_id, $input->getOption('hold')));

        sleep($input->getOption('hold'));

        $cancelling = $yobit->cancelOrder($order_id);

        $output->writeln(sprintf("<info>Order cancelling result : %s</info>",
                $cancelling['success'] == 1 ? 'OK' : 'ERROR'
            ));


    }
}