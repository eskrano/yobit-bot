<?php

namespace Trader\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Trader\Service\Yobit;

class VolCommand extends Command
{
    private $minOrdSum = 0.00010500; // 10k sat

    public function configure()
    {
        $this->setName('vol');
        $this->addOption('pair','p', InputOption::VALUE_REQUIRED, 'Pair', 'btc_usd');
        $this->addOption('min','m',InputOption::VALUE_OPTIONAL,'MIN',"0.00010500");
        $this->addOption('cost','c', InputOption::VALUE_REQUIRED, "COST");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $yobit = Yobit::getInstance();

        $pair = $input->getOption('pair');


        for ($i = 0; $i < 500; $i++) {
            $depth = $yobit->getPairDepth($pair);


            $depth = $depth[$pair];

            $_ask = $depth['asks'][0][0]; //left
            $_bids = $depth['bids'][0][0]; //right
            $spread = $_ask - $_bids;

           // $price = (($_ask + $_bids) / 2);
            //$price = 0.00000035;
            $price = $input->getOption('cost');
            $priceAsString = sprintf("%.8f", $price);

            $output->writeln(sprintf("<info>Price set %s</info>", $priceAsString));

            //$amount = $this->minOrdSum / $priceAsString;
            $amount = $input->getOption('min') / $priceAsString;
            $output->writeln(sprintf("<info>Placing Sell order</info>"));
            try {


                $bOrd = $yobit->trade($pair, Yobit::ORDER_TYPE_SELL, $priceAsString,
                    sprintf('%.8f', $amount)
                );
                $output->writeln(sprintf("<info>Sell order placed, ID: %d</info>", $bOrd['return']['order_id']));

                $output->writeln(sprintf("<info>Placing buy order</info>"));

                $sOrd = $yobit->trade($pair, Yobit::ORDER_TYPE_BUY, $priceAsString,
                    sprintf('%.8f', $amount)
                );

                $output->writeln(sprintf("<info>Buy order placed, ID: %d</info>", $sOrd['return']['order_id']));

                if ($sOrd['return']['remains'] == 0) {
                    $output->writeln(sprintf("<info>Buy order completed</info>"));
                }

                $output->writeln(sprintf("<info>Sleeping 1sec...</info>"));
                sleep(1);
            } catch (\Exception $e) {
                $output->writeln(sprintf("<error>Exception catched: %s</error>", $e->getMessage()));
                $output->writeln(sprintf("<info>Close all orders... Get active orders for this pair</info>"));

                $orders = $yobit->activeOrders($pair);

                $ids = array_keys($orders['return']);

                $output->writeln(sprintf("<info>Total %d active orders</info>", count($ids)));

                foreach ($ids as $id) {
                    $yobit->cancelOrder($id);
                }

                $output->writeln("<info>All orders closed</info>");

                $output->writeln("<info>Sleeping 2 sec. Respawn abbadon!</info>");
                $output->writeln("<error>================</error>");
                sleep(2);
            }
        }
    }
}