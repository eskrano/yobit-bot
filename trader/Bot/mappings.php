<?php

return  [
    '/test' => \Trader\Bot\Commands\TestCommand::class,
    '/way' => \Trader\Bot\Commands\WayCommand::class,
    '/chain' => \Trader\Bot\Commands\ChainCommand::class,
    'invest' => \Trader\Bot\Commands\InvestCommand::class,
    'admin' => \Trader\Bot\Commands\AdminTools::class,
    '/pa' => \Trader\Bot\Commands\PumpAnalyzer::class,
];