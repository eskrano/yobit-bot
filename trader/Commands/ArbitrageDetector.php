<?php

namespace Trader\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Trader\Service\Yobit;

class ArbitrageDetector extends Command
{
    private $combinableCurrencies = [
        'USD',
        'RUR',
        'DOGE',
        'WAVES',
        'BTC',
        'ETH',
    ];

    protected function configure()
    {
        $this->setName('arbitrabe:detect');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $yobit = Yobit::getInstance();


        $arbitrage_targets = file_exists($pairsPath = APP_DIR . '/arbitrage_pairs.json') ?
            file_get_contents($pairsPath) : false;


        if (! $arbitrage_targets) {
            $output->writeln("<error>Arbitrage pairs file not specified</error>");
            return;
        }

        $currencies = json_decode($arbitrage_targets, true);

        $pairs = [];

        foreach ($currencies as $currency) {
            // combine currencies
            foreach ($this->combinableCurrencies as $combinableCurrency) {
                $pairs[sprintf("%s_%s", $currency, $combinableCurrency)] = '';
            }
        }

        // add common depth

        $pairs = $this->hookPairs($pairs);


        $pairs = array_keys($pairs);

        $depth = $yobit->getPairsDepth($pairs);



    }

    private function hookPairs(array $pairs)
    {
        // мне влом пилить норм алгоритм , хуйну так костылем
        //btc
        $pairs["btc_usd"] = 0;
        $pairs["btc_rur"] = 0;
        $pairs["doge_btc"] = 0;
        $pairs["eth_btc"] = 0;
        $pairs["waves_btc"] = 0;

        //eth

        $pairs["eth_usd"] = 0;
        $pairs["eth_rur"] = 0;
        $pairs["doge_eth"] = 0;

    }

}