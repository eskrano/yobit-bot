<?php

namespace Trader;

use React\EventLoop\Factory;
use unreal4u\TelegramAPI\Abstracts\TraversableCustomType;
use unreal4u\TelegramAPI\HttpClientRequestHandler;
use unreal4u\TelegramAPI\Telegram\Methods\GetMe;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\User;
use unreal4u\TelegramAPI\TgLog;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use unreal4u\TelegramAPI\Telegram\Methods\GetUpdates;


class BotCommand extends Command
{
    protected function configure()
    {
        $this->setName('bot');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        define('BOT_TOKEN','482326974:AAEs0R5jZ4b54tqpRAJgvP4ie83qhlhs1TQ');
        define('A_USER_CHAT_ID', '@investdeveloper');

        $loop = Factory::create();
        $tgLog = new TgLog(BOT_TOKEN, new HttpClientRequestHandler($loop));
        $updates = new GetUpdates();

        $updatesPromise = $tgLog->performApiRequest($updates);
        $updatesPromise->then(function (TraversableCustomType $response) use ($output) {
            foreach ($response as $update) {
                $output->writeln(sprintf("<info>UpdateID:%d</info>", $update->update_id));
            }
        }, function (\Exception $e) use($output) {
            $output->writeln('<error>'. $e->getMessage() . '</error>');
        });

        $loop->run();

    }
}