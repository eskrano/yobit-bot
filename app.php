<?php

/**
 * Trader bot application
 */

define('APP_DIR', __DIR__ );

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;

$app = new Application();


/**
 * Initializing dotenv
 *
 * @since 1.0
 */
try {
    $dotenv = new \Dotenv\Dotenv(__DIR__);
    $dotenv->load();
} catch (Exception $e) {
    echo 'Boot error #1 . Check your .env file!';
    echo PHP_EOL;
    echo $e->getMessage();
    echo PHP_EOL;
    exit;
}

/**
 * Initializing commands
 */

/**
 * Install command
 */

$app->add(new \Trader\Commands\InstallCommand());

/**
 * Other commands
 */
$app->add(new \Trader\WatchCommand());
$app->add(new \Trader\YobitCommand());
$app->add(new \Trader\YobitPumpCommand());
$app->add(new \Trader\DummyCommand());
$app->add(new \Trader\BotCommand());
$app->add(new \Trader\ReadyCommand());
$app->add(new \Trader\PumpCommand());
$app->add(new \Trader\Commands\ImportPairs());
#$app->add(new \Trader\CodeTakerCommand());
// bots
$app->add(new \Trader\Bot\ArbBot());
$app->add(new \Trader\Commands\PumpDetector());
// arbitrage
$app->add(new \Trader\Commands\ArbitrageDetector());
$app->add(new \Trader\Commands\FlashOrderCommand());


// tools
$app->add(new \Trader\Commands\VolCommand());
$app->run();